module gitlab.com/coliss86/go-gallery

go 1.23.4

require (
	github.com/earthboundkid/versioninfo/v2 v2.24.1
	github.com/gorilla/mux v1.8.1
	github.com/meilisearch/meilisearch-go v0.30.0
	github.com/microcosm-cc/bluemonday v1.0.27
	github.com/spf13/cast v1.7.1
	github.com/stretchr/testify v1.10.0
)

require (
	github.com/andybalholm/brotli v1.1.1 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.1 // indirect
	github.com/gorilla/css v1.0.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.26.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
