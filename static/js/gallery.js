lazyload();

let gallery = [];

let anchors = document.getElementsByClassName("gallery");
for (let i = 0; i < anchors.length; i++) {
    gallery.push({
        filename: anchors[i].getAttribute('data-filename'),
        src: anchors[i].getAttribute('href'),
        imgpath: anchors[i].getAttribute('data-imgpath'),
        media: anchors[i].getAttribute('data-media'),
        "src-mp4": anchors[i].getAttribute('data-src-mp4')
    });

    anchors[i].addEventListener('click', (evt) =>  {
        evt.stopPropagation();
        evt.preventDefault();
        window.showGallery(i + 1);
    });
}

// store the current index
let slide = 0;
let gallery_launched = false;

window.showGallery = function(index){
    if (gallery_launched) {
        return;
    }

    function callImageToTag(event) {
        if (event && event.target && event.target.innerHTML) {
            const tag = event.target.innerHTML;
            let img = gallery[slide].imgpath;
            let img_name = img.substring(img.lastIndexOf("/") + 1);

            const found = tags[tag].find((element) => element === img_name);

            fetch("tag/" + tag, {
                method: found ? "DELETE" : "POST",
                body: JSON.stringify({ "img" : img }),
            }).then( (response) => {
                if (response.ok) {
                    this.classList.toggle("on");
                    if (found) {
                        removeItemAll(tags[tag], img_name);
                    } else {
                        if (!tags[tag].find((element) => element === img_name)) { // to avoid duplicate
                            tags[tag].push(img_name);
                        }
                    }
                }
            });
        }
    }

    const options = {
        control: "page, zoom, fullscreen, prev, next, close",
        animation: "none",

        // fires when gallery opens
        onshow: function(index){
            Spotlight.addControl("download-original", function(event){
                const link = /** @type {HTMLAnchorElement} */ (document.createElement("a"));
                const src = gallery[slide].imgpath;
                link.href = "download/" + src;
                link.download = src.substring(src.lastIndexOf("/") + 1);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            });

            Spotlight.addControlText("separator", "|", null);

            Object.entries(tags).forEach(([key, value]) => {
                Spotlight.addControlText("tag", key, callImageToTag);
            });
            gallery_launched = true;
        },
        // fires when gallery change to another page
        onchange: function(index, options){
            // store the current index for the button listener
            // the slide index start from 1 (as "page 1")
            slide = index - 1;
            Object.entries(tags).forEach(([key, value]) => {
                const tag = Spotlight.getControlText(key);
                let img = gallery[slide].imgpath;
                let img_name = img.substring(img.lastIndexOf("/") + 1);
                const found = value.find((element) => element === img_name);
                if (found) {
                    tag.classList.add("on");
                } else {
                    tag.classList.remove("on");
                }
            });
        },
        // fires when gallery is requested to close
        onclose: function(index){
            // remove the custom button, so you are able
            // to open next gallery without this custom control
            Spotlight.removeControl("download-original");

            Spotlight.removeControlText("|");
            Object.entries(tags).forEach(([key, value]) => {
                Spotlight.removeControlText(key);
            });
            gallery_launched = false;
        }
    };

    Spotlight.show(gallery, options, index);
};

document.body.onkeyup = function(e) {
    if (e.key === " " ||
        e.code === "Space"
    ) {
        window.showGallery(1);
    }
}

function removeItemAll(arr, value) {
    let i = 0;
    while (i < arr.length) {
        if (arr[i] === value) {
            arr.splice(i, 1);
        } else {
            ++i;
        }
    }
    return arr;
}

function new_tag() {
    const name= prompt("Nom du nouveau tag");
    if (name != null) {
        if (/\s/.test(name)) {
            alert("❌ Erreur : Le nom ne doit pas contenir d'espace")
            return false;
        }
        fetch("tags/" + name, {
            method: "POST"
        }).then( (response) => {
            if (response.ok) {
                location.reload();
            } else {
                alert("Erreur de création " + response.body);
            }
        });
    }
    return false;
}

function delete_tag(tag) {
    if (confirm("Êtes vous sûr de vouloir supprimer le tag \"" + tag + "\"?")) {
        fetch("tags/" + tag, {
            method: "DELETE"
        }).then( (response) => {
            if (response.ok) {
                location.reload();
            } else {
                alert("Erreur de suppression " + response.body);
            }
        });
    }
    return false;
}
