# 🖼️ Go gallery

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
![Gitlab Pipeline Status](https://gitlab.com/coliss86/go-gallery/badges/main/pipeline.svg?ignore_skipped=true)


Go gallery is a simple web gallery written in [golang](https://golang.org) to browse a folder hierarchy containing pictures. It displays them in a nice and elegant way.
It groups folders by month and day based on their names and displays them chronologically.
It also features a search engine powered by [Meilisearch](https://github.com/meilisearch/meilisearch).

A directory containing subfolders is displayed as follows:
![Root folder](/docs/root.jpg)

When going inside a folder with pictures, they are displayed in chronological order grouped by months:
![folders](/docs/folders.jpg)

Inside a folder containing `jpeg|jpg|gif|png|bmp` pictures and `mp4|m4v|mpeg|mpg|avi` videos and sort them by name and pictures first:
![inside](/docs/inside.jpg)

When clicking on a picture or video, it is displayed in fullscreen.
Keybinding are enabled in the navigation: <kbd>&rarr;</kbd>, <kbd>&larr;</kbd>, <kbd>ESC</kbd>

![Picture](/docs/fullscreen.jpg)

The folder hierarchy may be as follows:
```shell
├── 2022
│   ├── 03-01-dactyl
│   │   ├── 20220830_203541.jpg
│   │   ├── 20220830_203605.jpg
│   │   └── ...
│   ├── 03-07_Schwarz
│   └── ...
├── 2023
│   ├── 03-27-trousse
│   │   └── ...
│   ├── 2020-10-03_08-54_NOAA19_84
│   │   └── ...
└── tags
    ├── aImprimer
    └── clavier
```

## 👷 Installation

### 🔧 Getting from sources

`go-gallery` requires `golang` in version 1.23.4 at least.

To generate thumbnails, it uses [convert from ImageMagick](http://www.imagemagick.org/script/convert.php) for pictures and [ffmpeg](https://ffmpeg.org/) for videos.

To install them on debian with:
```console
sudo apt install imagemagick ffmpeg
```

On macos, use brew to install dependencies:
```console
brew install imagemagick ffmpeg 
```

Next, clone the project:
```console
git clone https://gitlab.com/coliss86/go-gallery.git/
```

Then build it:
```console
go build -o go-gallery
```

### 🐳 Using a container image (Docker)

A container image is built with [buildah](https://buildah.io/) and is available at `registry.gitlab.com/coliss86/go-gallery/snapshot:main`

## ⚡️ Start the application
To run it:
```console
./go-gallery -config <config file>
```

arguments:
* `config file`: *mandatory* path to the config file

> ⚠️Do not run it with `go run`, the app will not find the `static` folder, the template and so on

Here is an example of this config file:
```
# folder where pictures are stored
images=test/pictures

# folder with tags
tags=test/pictures/tags

# cache folder for temporary thumbnails
cache=test/cache

# sizes of thumbnail and preview pictures
thumbSize=150
smallSize=1200

# context root of the application if behind a reverse proxy
contextRoot=/photos

# host and port to listen to
listen=localhost
port=9090

# meilisearch url
meilisearchUrl=http://localhost:7700/
# token if meilisearch is in production mode
meilisearchToken=<token>
# index used to store data from the gallery
MeilisearchIndex=go-gallery
# activate meilisearch search
meilisearchActivated=true
```

## 🔧 Development

### ✅ Test

```console
go test ./... -v
```

### 📋 Lint

```console
golangci-lint run --fix -E gosec,goimports ./...
```

### 🐳 Buildah construction

Build:
```console
buildah bud -t go-gallery --layers --jobs 4 .
```

Run:
```console
podman run -it localhost/go-gallery:latest -version
```

## 📝 Reporting Issues

  * Please report issues on [Gitlab Issue Tracker](https://gitlab.com/coliss86/go-gallery/issues).
  * In your report, please provide steps to **reproduce** the issue.
  * Before reporting:
     * Make sure you are using the latest version of `main`.
     * Check existing open issues.
  * Merge requests, documentation requests, and enhancement ideas are welcome.

## 📄 License

"Go gallery" is distributed under [GNU GPL v3](http://www.gnu.org/licenses/gpl-3.0.en.html) license, see [LICENSE](LICENSE).

It also uses third-party libraries and programs:
  * woofunction icons ([GNU General Public License](http://www.gnu.org/licenses/gpl.html)): http://www.iconarchive.com/show/woofunction-icons-by-wefunction.html
  * Icon Aruzo - Dark, Author: sa-ki (License: Free for personal non-commercial use): http://sa-ki.deviantart.com
  * Play icon (CC0 1.0 Universal (CC0 1.0) Public Domain Dedication License): http://www.iconsdb.com/caribbean-blue-icons/video-play-3-icon.html
  * Gorilla web toolkit ([BSD licensed](https://opensource.org/licenses/BSD-2-Clause)): http://www.gorillatoolkit.org/
  * Spotlight (Apache-2.0 license): https://github.com/nextapps-de/spotlight (5160e7910f4cf27ac9300861ce795821059afba3)
  * Meilisearch (MIT license): https://github.com/meilisearch/meilisearch
  * To-be-continuous gitlab component https://to-be-continuous.gitlab.io/kicker/
