/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/earthboundkid/versioninfo/v2"

	"gitlab.com/coliss86/go-gallery/pkg/conf"
	"gitlab.com/coliss86/go-gallery/pkg/router"
	"gitlab.com/coliss86/go-gallery/pkg/search"
)

func main() {

	var confFilename string
	var indexAll bool
	var meilisearchActivated bool

	// flags declaration using flag package
	flag.StringVar(&confFilename, "config", "", "`config` file")
	flag.BoolVar(&indexAll, "i", false, "<true|false> index all folder")
	flag.BoolVar(&meilisearchActivated, "meilisearch", true, "Activate meilisearch")
	versioninfo.AddFlag(nil)

	flag.Parse()

	if confFilename == "" {
		flag.Usage()
		os.Exit(1)
	}

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	log.Println("Go-gallery https://gitlab.com/coliss86/go-gallery")
	log.Println("Version:", versioninfo.Short())

	checkAndFatal(conf.Read(confFilename))
	conf.Config.MeilisearchActivated = conf.Config.MeilisearchActivated && meilisearchActivated

	checkAndFatal(os.MkdirAll(path.Dir(conf.Config.Tags), os.ModePerm))

	// BaseDir = folder where the binary is stored, all resources are accessed relative to this folder
	baseDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	checkAndFatal(err)
	conf.Config.BaseDir = baseDir

	if conf.Config.MeilisearchActivated {
		checkAndFatal(search.CheckMeilisearch())
		if indexAll {
			checkAndFatal(search.IndexAllFolders())
		}
	}

	router.NewRouter()
	listen := fmt.Sprintf("%s:%d", conf.Config.Listen, conf.Config.Port)
	log.Println("Listening on", listen, "with contextRoot:", conf.Config.ContextRoot)

	srv := http.Server{
		Addr:              listen,
		WriteTimeout:      30 * time.Second,
		ReadTimeout:       1 * time.Second,
		IdleTimeout:       15 * time.Second,
		ReadHeaderTimeout: 2 * time.Second,
	}

	checkAndFatal(srv.ListenAndServe())
}

func checkAndFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
