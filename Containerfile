FROM docker.io/library/golang:1.23-alpine AS base

WORKDIR /app

RUN apk add git=2.47.2-r0 --no-cache && \
    rm -rf /var/cache/apk/*

COPY go.* .
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 go build -o go-gallery

FROM docker.io/library/alpine:3

RUN apk add imagemagick=7.1.1.41-r0 imagemagick-jpeg=7.1.1.41-r0 ffmpeg=6.1.2-r1 --no-cache && \
    rm -rf /var/cache/apk/*

WORKDIR /app
COPY --from=base /app/go-gallery go-gallery
COPY --from=base /app/static static
COPY --from=base /app/template template

USER 1000:1000
VOLUME ["/photos", "/cache"]
EXPOSE 9090

ENTRYPOINT ["/app/go-gallery"]
