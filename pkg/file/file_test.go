package file

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPathJoin(t *testing.T) {
	assert.Equal(t, "/album", PathJoin("/album"))
	assert.Equal(t, "/photos/album", PathJoin("/photos", "/album", "/"))
	assert.Equal(t, "/photos/album/subfolder", PathJoin("/photos", "/album", "/subfolder"))
	assert.Equal(t, "/photos/album", PathJoin("/photos", "/album"))
	assert.Equal(t, "/photos/album", PathJoin("/photos", "album"))
	assert.Equal(t, "/photos/album", PathJoin("/photos", "album", "./"))
	assert.Equal(t, "/photos/album", PathJoin("/photos", "../album"))
	assert.Equal(t, "/photos/album", PathJoin("/../photos", "../album"))
	assert.Equal(t, "/photos/album", PathJoin("/../photos", "../album", "../../../../"))
	assert.Equal(t, "/photos/album", PathJoin("/../photos", "album/..", "../../../../"))
}
