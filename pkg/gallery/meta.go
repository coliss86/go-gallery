package gallery

import (
	"fmt"
	"os"
	"path"
	"strings"

	"gitlab.com/coliss86/go-gallery/pkg/conf"
)

const metaProperties = "meta.properties"

func ReadFolderMeta(folder string) (Document, error) {
	document := Document{}

	fileMeta := path.Join(folder, metaProperties)

	_, err := os.Stat(fileMeta)
	if err != nil && os.IsNotExist(err) {
		return document, nil
	}

	meta, err := conf.ReadPropertiesFile(fileMeta)
	if err != nil {
		return document, fmt.Errorf("error while reading meta.properties from '%s': %w", folder, err)
	}
	if meta != nil {

		//title=Schwarz
		//descript=
		//date=07/01/2022
		//lieu=Paris
		//personnes=Schwarz, Koala
		//categories=Famille

		document.Title = meta["title"]
		document.Description = meta["descript"]
		if document.Description == "" {
			document.Description = meta["description"]
		}
		document.Date = meta["date"]
		document.Place = meta["place"]
		if document.Place == "" {
			document.Place = meta["lieu"]
		}

		peoples := meta["peoples"]
		if peoples == "" {
			peoples = meta["personnes"]
		}
		document.Peoples = splitAndTrim(peoples)
		document.Categories = splitAndTrim(meta["categories"])
		document.Path = strings.Replace(folder, conf.Config.Images, "", 1)
		document.Id = strings.Replace(document.Path, "/", "-", -1)
		document.Name = document.Path[strings.LastIndex(document.Path, "/")+1:]
		document.Parent = document.Path[:strings.LastIndex(document.Path, "/")]
	}

	return document, nil
}

func splitAndTrim(values string) []string {
	var categories []string
	if values != "" {
		split := strings.Split(values, ",")
		for i := range split {
			cat := strings.TrimSpace(split[i])
			if cat != "" {
				categories = append(categories, cat)
			}
		}
	}
	return categories
}
