package gallery

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRead_AlbumLegacy(t *testing.T) {
	doc, err := ReadFolderMeta("fixtures/album1")
	if err != nil {
		t.Error("Error while reading properties file")
	}
	assert.NotNil(t, doc)

	assert.Equal(t, "album1", doc.Name)
	assert.Equal(t, "long description", doc.Description)
	assert.Equal(t, "29/12/2006", doc.Date)
	assert.Equal(t, "Somewhere", doc.Place)
	assert.Equal(t, []string{"Schwarz", "Koala"}, doc.Peoples)
	assert.Equal(t, []string{"Cat1"}, doc.Categories)
	assert.Equal(t, "fixtures/album1", doc.Path)
	assert.Equal(t, "fixtures-album1", doc.Id)
	assert.Equal(t, "fixtures", doc.Parent)
}

func TestRead_Album(t *testing.T) {
	doc, err := ReadFolderMeta("fixtures/album2")
	if err != nil {
		t.Error("Error while reading properties file")
	}
	assert.NotNil(t, doc)

	assert.Equal(t, "album2", doc.Name)
	assert.Equal(t, "long description", doc.Description)
	assert.Equal(t, "29/12/2006", doc.Date)
	assert.Equal(t, "Somewhere", doc.Place)
	assert.Equal(t, []string{"Schwarz", "Koala"}, doc.Peoples)
	assert.Equal(t, []string{"Cat1", "Cat2"}, doc.Categories)
	assert.Equal(t, "fixtures/album2", doc.Path)
	assert.Equal(t, "fixtures-album2", doc.Id)
	assert.Equal(t, "fixtures", doc.Parent)
}

func TestRead_Album2(t *testing.T) {
	doc, err := ReadFolderMeta("fixtures/album3")
	if err != nil {
		t.Error("Error while reading properties file")
	}
	assert.NotNil(t, doc)

	assert.Equal(t, "album3", doc.Name)
	assert.Equal(t, "long description\nmulti lines", doc.Description)
	assert.Equal(t, "29/12/2006", doc.Date)
	assert.Equal(t, "Somewhere", doc.Place)
	assert.Equal(t, 0, len(doc.Peoples))
	assert.Equal(t, []string{"Cat1", "Cat2"}, doc.Categories)
	assert.Equal(t, "fixtures/album3", doc.Path)
	assert.Equal(t, "fixtures-album3", doc.Id)
	assert.Equal(t, "fixtures", doc.Parent)
}
