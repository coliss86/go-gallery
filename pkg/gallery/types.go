package gallery

type Document struct {
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Date        string   `json:"date"`
	Place       string   `json:"place"`
	Peoples     []string `json:"peoples"`
	Categories  []string `json:"categories"`
	Name        string   `json:"name"`
	Id          string   `json:"id"`
	Path        string   `json:"path"`
	Parent      string   `json:"parent"`
}
