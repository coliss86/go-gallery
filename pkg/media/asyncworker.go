package media

import (
	"log"
	"sync"

	"gitlab.com/coliss86/go-gallery/pkg/conf"
)

type Job interface {
	Work()
}

func workAndCatch(job Job) {
	defer func() {
		if err := recover(); err != nil {
			log.Printf("Error: %v", err)
		}
	}()
	job.Work()
}

func AsyncWork(jobs []Job) {
	chanJobs := make(chan Job, len(jobs))

	var wg sync.WaitGroup

	wg.Add(conf.Config.SmallResizeWorker)
	for w := range conf.Config.SmallResizeWorker {
		go func(id int, jobs <-chan Job, wg *sync.WaitGroup) {
			defer wg.Done()
			for job := range jobs {
				workAndCatch(job)
			}
		}(w, chanJobs, &wg)
	}

	// Add jobs
	for _, file := range jobs {
		chanJobs <- file
	}
	close(chanJobs)

	// Wait for completion
	wg.Wait()

}
