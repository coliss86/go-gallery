package media

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"sort"
	"strings"
	"time"

	"gitlab.com/coliss86/go-gallery/pkg/conf"

	"github.com/stretchr/testify/assert"

	"testing"
)

type Resize struct {
	file   string
	result chan string
}

func (r Resize) Work() {
	random, _ := rand.Int(rand.Reader, big.NewInt(500))
	random = random.Add(random, big.NewInt(500))
	time.Sleep(time.Duration(random.Int64()) * time.Millisecond)
	fmt.Printf("work on %s and wait %d ms\n", r.file, random)
	r.result <- r.file
}

func Test_AsyncWorker(t *testing.T) {
	// given
	conf.Config.SmallResizeWorker = 3
	files := []string{
		"2020-10-03_08-54_NOAA19_84-HVCT.png",
		"2020-10-03_08-54_NOAA19_84-JF.png",
		"2020-10-03_08-54_NOAA19_84-MCIR-PRECIP.png",
		"2020-10-03_08-54_NOAA19_84-MCIR.png",
		"2020-10-03_08-54_NOAA19_84-MSA.png",
		"2020-10-03_08-54_NOAA19_84-NO.png",
		"2020-10-03_08-54_NOAA19_84-THERM.png",
		"2020-10-03_08-54_NOAA19_84.png",
	}

	results := make(chan string, len(files))

	var jobs []Job
	for _, file := range files {
		jobs = append(jobs, Resize{file, results})
	}

	// when
	AsyncWork(jobs)

	// read results
	close(results)
	var actuals []string
	for elem := range results {
		actuals = append(actuals, elem)
	}
	sort.Strings(files)
	sort.Strings(actuals)

	assert.Equal(t, files, actuals)
}

type ResizeErr struct {
	file   string
	result chan string
}

func (r ResizeErr) Work() {
	random, _ := rand.Int(rand.Reader, big.NewInt(500))
	random = random.Add(random, big.NewInt(500))
	time.Sleep(time.Duration(random.Int64()) * time.Millisecond)
	if strings.Contains(r.file, "ko") {
		panic(fmt.Sprintf("aie on %s", r.file))
	} else {
		fmt.Printf("work on %s and wait %d ms\n", r.file, random)
	}
	r.result <- r.file
}

func Test_AsyncWorkerWithErrors(t *testing.T) {
	// given
	conf.Config.SmallResizeWorker = 3
	files := []string{
		"1_ok.png",
		"2_ko.png",
		"3_ok.png",
		"4_ok.png",
		"5_ko.png",
		"6_ok.png",
		"7_ok.png",
		"8_ko.png",
		"9_ok.png",
	}

	results := make(chan string, len(files))

	var jobs []Job
	for _, file := range files {
		jobs = append(jobs, ResizeErr{file, results})
	}

	// when
	AsyncWork(jobs)

	// read results
	close(results)
	var actuals []string
	for elem := range results {
		actuals = append(actuals, elem)
	}
	sort.Strings(actuals)

	assert.Equal(t, []string{"1_ok.png", "3_ok.png", "4_ok.png", "6_ok.png", "7_ok.png", "9_ok.png"}, actuals)
}
