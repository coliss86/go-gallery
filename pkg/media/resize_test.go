/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package media

import (
	"fmt"
	"os"
	"path"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/coliss86/go-gallery/pkg/conf"
)

func Test_ResizeImage(t *testing.T) {
	// given
	conf.Config.Cache = "fixtures/cache"
	defer os.RemoveAll("fixtures/cache")

	_ = os.RemoveAll("fixtures/cache")

	// when
	dstName, errResize := ResizeImage("fixtures/Red_Panda.jpg", "dst.jpg", "folder1", func(srcName string, dstName string) []Command {
		assert.Equal(t, "fixtures/Red_Panda.jpg", srcName)
		// fixtures/cache/tmpfile-3300695861.jpeg
		assert.True(t, strings.HasPrefix(dstName, "fixtures/cache/tmpfile-"))
		assert.True(t, strings.HasSuffix(dstName, ".jpeg"))
		infoDst, err := os.Stat(path.Dir(dstName))
		assert.Nil(t, err)
		assert.True(t, infoDst.IsDir())
		return []Command{
			{"cp", []string{srcName, dstName}},
		}
	})

	// then
	assert.Nil(t, errResize)
	infoDst, err := os.Stat(dstName)
	assert.Nil(t, err)
	assert.True(t, infoDst.Size() > 200)
	assert.Equal(t, "fixtures/cache/folder1/dst.jpg", dstName)
}

func Test_ConvertThumbnail(t *testing.T) {
	// given
	conf.Config.ThumbSize = 200

	// when
	cmds := ConvertThumbnail("picture/src.jpeg", "test/cache/tmpfile-2913046891.jpeg")

	// then
	assert.Equal(t, []Command{{cmd: "convert", args: []string{"-auto-orient", "-strip", "-define", "jpeg:size=250x250", "-thumbnail", "200x200^", "-gravity", "center", "-extent", "200x200^", "picture/src.jpeg", "test/cache/tmpfile-2913046891.jpeg"}}}, cmds)
}

func Test_ConvertSmall(t *testing.T) {
	// when
	cmds := ConvertSmall("picture/src.jpeg", "test/cache/tmpfile-2913046891.jpeg")

	// then
	expectCmds := []Command{
		{"convert", []string{"-auto-orient", "-strip", "-quality", "80", "-resize", "x0", "picture/src.jpeg", "test/cache/tmpfile-2913046891.jpeg"}},
	}

	assert.Equal(t, expectCmds, cmds)
}

func Test_ConvertThumbnailVideo(t *testing.T) {
	// when
	cmds := ConvertThumbnailVideo("picture/src.jpeg", "test/cache/tmpfile-2913046891.jpeg")

	// then
	expectCmds := []Command{
		{"ffmpeg", []string{"-i", "picture/src.jpeg", "-ss", "00:00:01.000", "-vframes", "1", "test/cache/tmpfile-2913046891_vidtmp.jpeg"}},
		{"convert", []string{"test/cache/tmpfile-2913046891_vidtmp.jpeg", "-auto-orient", "-strip", "-quality", "92", "-define", "jpeg:size=250x250", "-thumbnail", "200x200^", "-gravity", "center", "-extent", "200x200^", "static/img/video-play.png", "-composite", "test/cache/tmpfile-2913046891.jpeg"}},
		{"rm", []string{"-f", "test/cache/tmpfile-2913046891_vidtmp.jpeg"}},
	}
	assert.Equal(t, expectCmds, cmds)
}

func Test_extensionRE(t *testing.T) {
	// when
	matches := extensionRE.FindStringSubmatch("picture/src.jpeg")
	assert.Equal(t, "picture/src_tmp.jpeg", fmt.Sprintf("%s_tmp.%s", matches[1], matches[2]))

	// when
	matches = extensionRE.FindStringSubmatch("picture/filename.src.jpeg")
	assert.Equal(t, "picture/filename.src_tmp.jpeg", fmt.Sprintf("%s_tmp.%s", matches[1], matches[2]))

}
