/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package media

import (
	"bytes"
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path"
	"regexp"

	"gitlab.com/coliss86/go-gallery/pkg/conf"
	"gitlab.com/coliss86/go-gallery/pkg/file"
)

type Command struct {
	cmd  string
	args []string
}

type Resizer func(srcName string, dstName string) []Command

var extensionRE = regexp.MustCompile(`(.*)\.(.*)$`)

func ResizeImage(imageFilename string, imageCacheFilename string, cacheName string, fn Resizer) (string, error) {
	srcName := file.PathJoin(conf.Config.Images, imageFilename)
	dstName := file.PathJoin(conf.Config.Cache, cacheName, imageCacheFilename)

	infoSrc, err := os.Stat(srcName)
	if err != nil && os.IsNotExist(err) || infoSrc.IsDir() {
		return "", fs.ErrNotExist
	}

	err = os.MkdirAll(path.Dir(dstName), os.ModePerm)
	if err != nil {
		return "", err
	}

	infoDst, err := os.Stat(dstName)
	if err != nil && os.IsNotExist(err) || infoSrc.ModTime().After(infoDst.ModTime()) {

		f, err := os.CreateTemp(conf.Config.Cache, "tmpfile-*.jpeg")
		if err != nil {
			return "", err
		}
		defer f.Close()
		defer os.Remove(f.Name())

		for _, cmd := range fn(srcName, f.Name()) {
			errCmd := execCmd(cmd)
			if errCmd != nil {
				return "", fmt.Errorf("error while resizing '%s' to '%s': %s\n", imageFilename, cacheName, errCmd)
			}
		}

		// mv tmp.jpeg dstName.jpeg
		errRen := os.Rename(f.Name(), dstName)
		if errRen != nil {
			return "", fmt.Errorf("can't move '%s' to '%s': %s", f.Name(), dstName, errRen)
		}
	}
	return dstName, nil
}

func ConvertThumbnail(srcName string, dstName string) []Command {
	// convert -define jpeg:size=200x200 original.jpeg  -thumbnail 100x100^ -gravity center -extent 100x100  thumbnail.jpeg
	size := conf.Config.ThumbSize

	return []Command{{"convert", []string{
		"-auto-orient", "-strip",
		"-define", fmt.Sprintf("jpeg:size=%dx%d", size+50, size+50),
		"-thumbnail", fmt.Sprintf("%dx%d^", size, size),
		"-gravity", "center",
		"-extent", fmt.Sprintf("%dx%d^", size, size),
		srcName, dstName,
	}}}
}

func ConvertSmall(srcName string, dstName string) []Command {
	// convert ... srcName.jpeg dstName.jpeg
	return []Command{{"convert", []string{
		"-auto-orient",
		"-strip", "-quality", "80", // slightly decrease quality to optimise size
		"-resize", fmt.Sprintf("x%d", conf.Config.SmallSize),
		srcName, dstName,
	}}}
}

func ConvertThumbnailVideo(srcName string, dstName string) []Command {
	size := conf.Config.ThumbSize

	cmds := make([]Command, 3)
	matches := extensionRE.FindStringSubmatch(dstName)
	tmpName := fmt.Sprintf("%s_vidtmp.%s", matches[1], matches[2])

	// ffmpeg -i 20240923_203445.mp4 -ss 00:00:01.000 -vframes 1 20240923_203445_thumbnail.jpg
	cmds[0] = Command{"ffmpeg", []string{
		"-i", srcName,
		"-ss", "00:00:01.000", "-vframes", "1",
		tmpName,
	}}

	// magick convert video_tmp.jpg -auto-orient -define "jpeg:size=250x250" -thumbnail "200x200^" -gravity center -extent "200x200^" static/img/video-play.png -composite dst.jpeg
	cmds[1] = Command{"convert", []string{
		tmpName, "-auto-orient", "-strip", "-quality", "92",
		"-define", fmt.Sprintf("jpeg:size=%dx%d", size+50, size+50),
		"-thumbnail", fmt.Sprintf("%dx%d^", size, size),
		"-gravity", "center",
		"-extent", fmt.Sprintf("%dx%d^", size, size),
		"static/img/video-play.png", "-composite",
		dstName,
	}}

	// rm tmp
	cmds[2] = Command{"rm", []string{"-f", tmpName}}

	return cmds
}

func execCmd(cmd Command) error {
	cmdPath, _ := exec.LookPath(cmd.cmd)
	execution := exec.Command(cmdPath, cmd.args...)
	execution.Env = os.Environ()
	execution.Env = append(execution.Env, "MAGICK_TEMPORARY_PATH="+conf.Config.Cache)
	var stdout, stderr bytes.Buffer
	execution.Stdout = &stdout
	execution.Stderr = &stderr
	err := execution.Run()
	if err != nil {
		return fmt.Errorf("error while running command: '%s %s' :%w, stdout: %s, stderr: %s", cmdPath, execution, err, stdout.String(), stderr.String())
	}
	return nil
}
