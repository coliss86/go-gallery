/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package router

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/coliss86/go-gallery/pkg/conf"
	"gitlab.com/coliss86/go-gallery/pkg/handler"
)

func NewRouter() *mux.Router {

	router := mux.NewRouter()

	router.Use(panicRecovery)

	if conf.Config.MeilisearchActivated {
		router.HandleFunc(conf.Config.ContextRoot+"/index/all", handler.IndexAll)
		router.HandleFunc(conf.Config.ContextRoot+"/index/{folder:.*}", handler.IndexOne)
	}
	router.HandleFunc(conf.Config.ContextRoot+"/ui/search", handler.Search).Queries("q", "{q}")

	router.HandleFunc(conf.Config.ContextRoot+"/ui/{folder:.*}", handler.RenderUI)
	router.HandleFunc(conf.Config.ContextRoot+"/ui", handler.RenderUI)
	router.HandleFunc(conf.Config.ContextRoot+"/tag/{tag}", handler.ManageImg).Methods(http.MethodPost, http.MethodDelete)
	router.HandleFunc(conf.Config.ContextRoot+"/tags/{tag}", handler.ManageTag).Methods(http.MethodPost, http.MethodDelete)
	router.HandleFunc(conf.Config.ContextRoot+"/thumb/{img:.*}", handler.RenderThumb)
	router.HandleFunc(conf.Config.ContextRoot+"/small/{img:.*}", handler.RenderSmall)
	router.HandleFunc(conf.Config.ContextRoot+"/download/{img:.*}", handler.RenderDownload)
	router.HandleFunc(conf.Config.ContextRoot+"/img/{img:.*}", handler.RenderImg)

	router.HandleFunc(conf.Config.ContextRoot+"/", func(w http.ResponseWriter, r *http.Request) {
		//redirecting to /ui/ when / is called
		http.Redirect(w, r, conf.Config.ContextRoot+"/ui/", http.StatusMovedPermanently)
	})
	http.Handle(conf.Config.ContextRoot+"/", router)

	http.Handle(conf.Config.ContextRoot+"/static/", http.StripPrefix(conf.Config.ContextRoot+"/static/", http.FileServer(http.Dir(conf.Config.BaseDir+"/static"))))

	return router
}

func panicRecovery(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				log.Printf("Error: %v", err)
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = w.Write([]byte(`{"error":"our server got panic"}`))
			}
		}()
		h.ServeHTTP(w, r)
	})
}
