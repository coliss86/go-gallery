/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"errors"
	"fmt"
	"io/fs"
	"net/http"
	"os"
	"regexp"

	"github.com/gorilla/mux"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/coliss86/go-gallery/pkg/conf"
	"gitlab.com/coliss86/go-gallery/pkg/file"
	"gitlab.com/coliss86/go-gallery/pkg/media"
)

var urlImgRE = regexp.MustCompile("(.*)/([^/]*)/?")

func RenderImg(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	policy := bluemonday.UGCPolicy()
	image := policy.Sanitize(vars["img"])

	serveFile(resp, req, file.PathJoin(conf.Config.Images, image))
}

func RenderThumb(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	policy := bluemonday.UGCPolicy()
	imageFilename := policy.Sanitize(vars["img"])
	matches := videoRE.FindStringSubmatch(imageFilename)
	if len(matches) > 0 {
		dstCacheFilename := fmt.Sprintf("%s_video.jpg", matches[1])
		renderImageResize(resp, req, imageFilename, dstCacheFilename, conf.ThumbCacheDir, media.ConvertThumbnailVideo)
	} else {
		renderImageResize(resp, req, imageFilename, imageFilename, conf.ThumbCacheDir, media.ConvertThumbnail)
	}
}

func RenderSmall(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	policy := bluemonday.UGCPolicy()
	imageFilename := policy.Sanitize(vars["img"])
	renderImageResize(resp, req, imageFilename, imageFilename, conf.SmallCacheDir, media.ConvertSmall)
}

func renderImageResize(resp http.ResponseWriter, req *http.Request, imageFilename string, imageCacheFilename string, cacheName string, fn media.Resizer) {
	ic, err := media.ResizeImage(imageFilename, imageCacheFilename, cacheName, fn)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			// Return a 404 if the file doesn't exist
			http.NotFound(resp, req)
			return
		} else {
			PanicOnErr(err, "can't resize image '%s'", imageFilename)
		}
	}

	serveFile(resp, req, ic)
}

func RenderDownload(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	policy := bluemonday.UGCPolicy()
	image := policy.Sanitize(vars["img"])

	matches := urlImgRE.FindStringSubmatch(image)
	title := ""
	if len(matches) > 0 {
		title = matches[2]
	} else {
		title = image
	}
	resp.Header().Set("Content-Disposition", "attachment; filename="+title)
	serveFile(resp, req, file.PathJoin(conf.Config.Images, image))
}

func serveFile(resp http.ResponseWriter, req *http.Request, file string) {
	// Return a 404 if the file doesn't exist
	info, err := os.Stat(file)
	if err != nil && os.IsNotExist(err) || info.IsDir() {
		http.NotFound(resp, req)
		return
	}

	fileOs, err := os.Open(file)
	PanicOnErr(err, "can't open file '%s'", file)

	defer fileOs.Close()
	http.ServeContent(resp, req, info.Name(), info.ModTime(), fileOs)
}
