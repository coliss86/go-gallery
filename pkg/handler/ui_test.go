package handler

import (
	"encoding/base64"
	"sort"
	"strings"
	"testing"

	"github.com/meilisearch/meilisearch-go"

	"github.com/stretchr/testify/assert"
	"gitlab.com/coliss86/go-gallery/pkg/conf"
)

func Test_readFolder_rootFolder(t *testing.T) {
	// given
	folderCurrent := ""
	folderFullPath := "fixtures"
	conf.Config.ThumbSize = 3
	conf.Config.ContextRoot = "/photos"
	conf.Config.Images = "fixtures/"
	conf.Config.Tags = "fixtures/tags/"

	// when
	data := readFolderAndPrepareData(folderCurrent, folderFullPath, "")

	// then
	assert.Equal(t, "3", data.ThumbSize)
	assert.Equal(t, "/photos", data.ContextRoot)
	assert.Equal(t, true, data.Version != "")
	assert.Equal(t, "", data.Query)
	assert.NotNil(t, data.GroupsName)
	assert.Equal(t, "", data.Folder)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "", data.Next)
	assert.Equal(t, 1, len(keys(data.Folders)))
	assert.Equal(t, []string{""}, keys(data.Folders[""]))
	folders := []Item{
		{Link: "2022", Name: "2022", LongName: "", Thumb: "", Class: "folder-normal", Total: 1, Day: "", Month: "", Year: ""},
		{Link: "2023", Name: "2023", LongName: "", Thumb: "", Class: "folder-normal", Total: 4, Day: "", Month: "", Year: ""},
		{Link: "2024", Name: "2024", LongName: "", Thumb: "", Class: "folder-normal", Total: 0, Day: "", Month: "", Year: ""},
		{Link: "tags", Name: "tags", LongName: "", Thumb: "", Class: "folder-normal", Total: 2, Day: "", Month: "", Year: ""},
	}
	assert.Equal(t, folders, data.Folders[""][""])

	assert.Equal(t, 0, len(data.Videos))
	assert.Equal(t, 0, len(data.Pictures))
	assert.Equal(t, "Gallery", data.Title)
	assert.Equal(t, "Dossiers", data.Message)
	assert.Equal(t, 0, data.Total)
	assert.True(t, data.IsRoot)

	assertTags(t, data)
}

func Test_readFolder_folder2023(t *testing.T) {
	// given
	folderCurrent := "2023"
	folderFullPath := "fixtures/2023"
	conf.Config.ThumbSize = 3
	conf.Config.ContextRoot = "/photos"
	conf.Config.Images = "fixtures/"
	conf.Config.Tags = "fixtures/tags/"

	// when
	data := readFolderAndPrepareData(folderCurrent, folderFullPath, "")

	// then
	assert.Equal(t, "3", data.ThumbSize)
	assert.Equal(t, "/photos", data.ContextRoot)
	assert.Equal(t, true, data.Version != "")
	assert.Equal(t, "", data.Query)
	assert.NotNil(t, data.GroupsName)
	assert.Equal(t, "2023/", data.Folder)
	assert.Equal(t, []Item{{Name: "2023"}}, data.Breadcrumb)
	assert.Equal(t, "2022", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "2024", data.Next)
	assert.Equal(t, []string{"01", "03", "06"}, keys(data.Folders))

	assert.Equal(t, []string{""}, keys(data.Folders["01"]))
	folder01 := []Item{
		{Link: "2023/01-04_AlbumNumber1", Name: "04", LongName: "Album&nbsp;number&nbsp;1", Thumb: "2023-01-04_AlbumNumber1_photo_01.jpg", Class: "folder-short folder-thumb", Total: 3, Day: "", Month: "01", Year: "2023"},
		{Link: "2023/01-14_AlbumNumber2", Name: "14", LongName: "Album&nbsp;number&nbsp;2", Thumb: "2023-01-14_AlbumNumber2_photo_01.jpg", Class: "folder-short folder-thumb", Total: 3, Day: "", Month: "01", Year: "2023"},
	}
	assert.Equal(t, folder01, data.Folders["01"][""])

	assert.Equal(t, []string{""}, keys(data.Folders["03"]))
	folder03 := []Item{
		{Link: "2023/03-12_AnotherAlbum", Name: "12", LongName: "Another&nbsp;Album", Thumb: "2023-03-12_AnotherAlbum_photo_01.jpg", Class: "folder-short folder-thumb", Total: 2, Day: "", Month: "03", Year: "2023"},
	}
	assert.Equal(t, folder03, data.Folders["03"][""])

	assert.Equal(t, []string{""}, keys(data.Folders["06"]))
	folder06 := []Item{
		{Link: "2023/06-09_MyAlbum", Name: "09", LongName: "My&nbsp;Album", Thumb: "2023-06-09_MyAlbum_photo_1.jpg", Class: "folder-short folder-thumb", Total: 5, Day: "", Month: "06", Year: "2023"},
	}
	assert.Equal(t, folder06, data.Folders["06"][""])
	assert.Equal(t, 0, len(data.Videos))
	assert.Equal(t, 0, len(data.Pictures))
	assert.Equal(t, "2023", data.Title)
	assert.Equal(t, "", data.Message)
	assert.Equal(t, 0, data.Total)
	assert.False(t, data.IsRoot)

	assertTags(t, data)
}

func Test_readFolder_folder2024(t *testing.T) {
	// given
	folderCurrent := "2024"
	folderFullPath := "fixtures/2024"
	conf.Config.ThumbSize = 3
	conf.Config.ContextRoot = "/photos"
	conf.Config.Images = "fixtures/"
	conf.Config.Tags = "fixtures/tags/"

	// when
	data := readFolderAndPrepareData(folderCurrent, folderFullPath, "")

	// then
	assert.Equal(t, "3", data.ThumbSize)
	assert.Equal(t, "/photos", data.ContextRoot)
	assert.Equal(t, true, data.Version != "")
	assert.Equal(t, "", data.Query)
	assert.NotNil(t, data.GroupsName)
	assert.Equal(t, "2024/", data.Folder)
	assert.Equal(t, []Item{{Name: "2024"}}, data.Breadcrumb)
	assert.Equal(t, "2023", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "tags", data.Next)
	assert.Equal(t, 0, len(data.Folders))
	assert.Equal(t, 0, len(data.Videos))
	assert.Equal(t, 0, len(data.Pictures))
	assert.Equal(t, "2024", data.Title)
	assert.Equal(t, "", data.Message)
	assert.Equal(t, 0, data.Total)
	assert.False(t, data.IsRoot)

	assertTags(t, data)
}

func Test_readFolder_video(t *testing.T) {
	// given
	folderCurrent := "2023/01-14_AlbumNumber2"
	folderFullPath := "fixtures/2023/01-14_AlbumNumber2"
	conf.Config.ThumbSize = 3
	conf.Config.ContextRoot = "/photos"
	conf.Config.Images = "fixtures/"
	conf.Config.Tags = "fixtures/tags/"

	// when
	data := readFolderAndPrepareData(folderCurrent, folderFullPath, "")

	// then
	assert.Equal(t, "3", data.ThumbSize)
	assert.Equal(t, "/photos", data.ContextRoot)
	assert.Equal(t, true, data.Version != "")
	assert.Equal(t, "", data.Query)
	assert.NotNil(t, data.GroupsName)
	assert.Equal(t, "2023/01-14_AlbumNumber2/", data.Folder)
	assert.Equal(t, []Item{{Name: "2023", Link: "2023"}, {Name: "01-14_AlbumNumber2"}}, data.Breadcrumb)
	assert.Equal(t, "2023/01-04_AlbumNumber1", data.Previous)
	assert.Equal(t, "2023", data.Up)
	assert.Equal(t, "2023/03-12_AnotherAlbum", data.Next)
	assert.Equal(t, 0, len(data.Folders))

	assert.Equal(t, []string{"video.mp4"}, data.Videos)
	assert.Equal(t, []string{"2023-01-14_AlbumNumber2_photo_01.jpg", "2023-01-14_AlbumNumber2_photo_02.jpg", "2023-01-14_AlbumNumber2_photo_03.jpg"}, data.Pictures)
	assert.Equal(t, "Album number 2", data.Title)
	assert.Equal(t, "", data.Message)
	assert.Equal(t, 4, data.Total)
	assert.False(t, data.IsRoot)

	assertTags(t, data)
}

func Test_readFolder_novideo(t *testing.T) {
	// given
	folderCurrent := "2023/01-04_AlbumNumber1"
	folderFullPath := "fixtures/2023/01-04_AlbumNumber1"
	conf.Config.ThumbSize = 3
	conf.Config.ContextRoot = "/photos"
	conf.Config.Images = "fixtures/"
	conf.Config.Tags = "fixtures/tags/"

	// when
	data := readFolderAndPrepareData(folderCurrent, folderFullPath, "")

	// then
	assert.Equal(t, "3", data.ThumbSize)
	assert.Equal(t, "/photos", data.ContextRoot)
	assert.Equal(t, true, data.Version != "")
	assert.Equal(t, "", data.Query)
	assert.NotNil(t, data.GroupsName)
	assert.Equal(t, "2023/01-04_AlbumNumber1/", data.Folder)
	assert.Equal(t, []Item{{Name: "2023", Link: "2023"}, {Name: "01-04_AlbumNumber1"}}, data.Breadcrumb)
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "2023", data.Up)
	assert.Equal(t, "2023/01-14_AlbumNumber2", data.Next)
	assert.Equal(t, 0, len(data.Folders))

	assert.Equal(t, 0, len(data.Videos))
	assert.Equal(t, []string{"2023-01-04_AlbumNumber1_photo_01.jpg", "2023-01-04_AlbumNumber1_photo_02.jpg", "2023-01-04_AlbumNumber1_photo_03.jpg"}, data.Pictures)
	assert.Equal(t, "Album number 1", data.Title)
	assert.Equal(t, "", data.Message)
	assert.Equal(t, 3, data.Total)
	assert.False(t, data.IsRoot)

	assertTags(t, data)
}

func Test_readFolder_folderTags(t *testing.T) {
	// given
	folderCurrent := "tags"
	folderFullPath := "fixtures/tags"
	conf.Config.ThumbSize = 3
	conf.Config.ContextRoot = "/photos"
	conf.Config.Images = "fixtures/"
	conf.Config.Tags = "fixtures/tags/"

	// when
	data := readFolderAndPrepareData(folderCurrent, folderFullPath, "")

	// then
	assert.Equal(t, "3", data.ThumbSize)
	assert.Equal(t, "/photos", data.ContextRoot)
	assert.Equal(t, true, data.Version != "")
	assert.Equal(t, "", data.Query)
	assert.NotNil(t, data.GroupsName)
	assert.Equal(t, "tags/", data.Folder)
	assert.Equal(t, []Item{{Name: "tags"}}, data.Breadcrumb)
	assert.Equal(t, "2024", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "", data.Next)
	assert.Equal(t, 1, len(data.Folders))

	assert.Equal(t, []string{""}, keys(data.Folders[""]))
	folders := []Item{
		{Link: "tags/loooonnngggNaaaame", Name: "loooonnngggNaaaame", LongName: "", Thumb: "2023-03-12_AnotherAlbum_photo_01.jpg", Class: "folder-long folder-thumb", Total: 1, Day: "", Month: "", Year: ""},
		{Link: "tags/toPrint", Name: "toPrint", LongName: "", Thumb: "2022-12-01_LastMonth_photo_1.jpg", Class: "folder-normal folder-thumb", Total: 2, Day: "", Month: "", Year: ""},
	}
	assert.Equal(t, folders, data.Folders[""][""])

	assert.Equal(t, 0, len(data.Videos))
	assert.Equal(t, 0, len(data.Pictures))
	assert.Equal(t, "Tags", data.Title)
	assert.Equal(t, "Tags", data.Message)
	assert.Equal(t, 0, data.Total)

	tagsPictures := map[string][]string{"loooonnngggNaaaame": {"2023-03-12_AnotherAlbum_photo_01.jpg"}, "toPrint": {"2022-12-01_LastMonth_photo_1.jpg", "2023-01-04_AlbumNumber1_photo_03.jpg"}}
	assert.Equal(t, tagsPictures, data.TagsPictures)
	assert.Equal(t, "tags/", data.TagsLink)
	assert.Equal(t, true, data.IsInTagFolder)
	assert.Equal(t, true, data.IsRootTagFolder)
	assert.True(t, data.IsRoot)
}

func Test_readFolder_withSearchResults(t *testing.T) {
	// given
	folderCurrent := "2023/01-14_AlbumNumber2"
	folderFullPath := "fixtures/2023/01-14_AlbumNumber2"
	conf.Config.ThumbSize = 3
	conf.Config.ContextRoot = "/photos"
	conf.Config.Images = "fixtures/"
	conf.Config.Tags = "fixtures/tags/"
	expectedSearchResults := base64.URLEncoding.EncodeToString([]byte("my_query,2018/05-06_Other2,2023/01-04_AlbumNumber1,2023/01-14_AlbumNumber2,2022/12-01_LastMonth,2019/06-06_Other,"))

	// when
	data := readFolderAndPrepareData(folderCurrent, folderFullPath, expectedSearchResults)

	// then
	assert.Equal(t, "3", data.ThumbSize)
	assert.Equal(t, "/photos", data.ContextRoot)
	assert.Equal(t, true, data.Version != "")
	assert.NotNil(t, data.GroupsName)
	assert.Equal(t, "my_query", data.Query)
	assert.Equal(t, "2023/01-14_AlbumNumber2/", data.Folder)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "2023/01-04_AlbumNumber1"+searchQueryUri+expectedSearchResults, data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "2022/12-01_LastMonth"+searchQueryUri+expectedSearchResults, data.Next)
	assert.Equal(t, 0, len(data.Folders))

	assert.Equal(t, []string{"video.mp4"}, data.Videos)
	assert.Equal(t, []string{"2023-01-14_AlbumNumber2_photo_01.jpg", "2023-01-14_AlbumNumber2_photo_02.jpg", "2023-01-14_AlbumNumber2_photo_03.jpg"}, data.Pictures)
	assert.Equal(t, "Album number 2", data.Title)
	assert.Equal(t, "", data.Message)
	assert.Equal(t, 4, data.Total)
	assert.False(t, data.IsRoot)

	assertTags(t, data)
}

func Test_readSearch_fewResults(t *testing.T) {
	// given
	conf.Config.ThumbSize = 3
	conf.Config.ContextRoot = "/photos"
	conf.Config.Images = "fixtures/"
	conf.Config.Tags = "fixtures/tags/"

	response := meilisearch.SearchResponse{
		Hits: []interface{}{
			map[string]interface{}{
				"parent": "2023", "name": "01-14_AlbumNumber2",
			},
			map[string]interface{}{
				"parent": "2022", "name": "12-01_LastMonth",
			},
			map[string]interface{}{
				"parent": "2023", "name": "03-12_AnotherAlbum",
			},
			map[string]interface{}{
				"parent": "2023", "name": "06-09_MyAlbum",
			},
		},
		EstimatedTotalHits: 10,
		Offset:             0,
		Limit:              20,
	}

	// when
	data := readSearchResultsAndPrepareData("my_query", &response)

	// then
	assert.Equal(t, "3", data.ThumbSize)
	assert.Equal(t, "/photos", data.ContextRoot)
	assert.Equal(t, true, data.Version != "")
	assert.Equal(t, "my_query", data.Query)
	assert.Equal(t, false, data.ConfByDay)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "", data.Next)
	assert.Equal(t, "tags/", data.TagsLink)
	assert.Equal(t, false, data.IsInTagFolder)
	assert.Equal(t, false, data.IsRootTagFolder)
	assert.Equal(t, "", data.Folder)
	assert.Equal(t, 10, data.Total)
	assert.Equal(t, "Recherche my_query", data.Title)
	assert.Equal(t, "10 résultats de recherche 'my_query' :", data.Message)
	assert.True(t, data.IsRoot)
	assert.Equal(t, []string{"2022", "2023"}, keys(data.GroupsName))

	assert.Equal(t, []string{"2022", "2023"}, keys(data.Folders))
	assert.Equal(t, []string{""}, keys(data.Folders["2022"]))
	folders := []Item{
		{Link: "2022/12-01_LastMonth", Name: "01", LongName: "Last&nbsp;month", Thumb: "2022-12-01_LastMonth_photo_1.jpg", Class: "folder-short folder-thumb", Total: 1, Day: "", Month: "12", Year: "2022"},
	}
	assert.Equal(t, folders, data.Folders["2022"][""])

	assert.Equal(t, []string{""}, keys(data.Folders["2023"]))
	folders = []Item{
		{Link: "2023/01-14_AlbumNumber2", Name: "14", LongName: "Album&nbsp;number&nbsp;2", Thumb: "2023-01-14_AlbumNumber2_photo_01.jpg", Class: "folder-short folder-thumb", Total: 3, Day: "", Month: "01", Year: "2023"},
		{Link: "2023/03-12_AnotherAlbum", Name: "12", LongName: "Another&nbsp;Album", Thumb: "2023-03-12_AnotherAlbum_photo_01.jpg", Class: "folder-short folder-thumb", Total: 2, Day: "", Month: "03", Year: "2023"},
		{Link: "2023/06-09_MyAlbum", Name: "09", LongName: "My&nbsp;Album", Thumb: "2023-06-09_MyAlbum_photo_1.jpg", Class: "folder-short folder-thumb", Total: 5, Day: "", Month: "06", Year: "2023"},
	}
	assert.Equal(t, folders, data.Folders["2023"][""])

	assertSearchResults(t, data.SearchResults, "my_query,2023/01-14_AlbumNumber2,2022/12-01_LastMonth,2023/03-12_AnotherAlbum,2023/06-09_MyAlbum,")
}

func Test_readSearch_noResults(t *testing.T) {
	// given
	conf.Config.ThumbSize = 3
	conf.Config.ContextRoot = "/photos"
	conf.Config.Images = "fixtures/"
	conf.Config.Tags = "fixtures/tags/"

	response := meilisearch.SearchResponse{
		Hits:               []interface{}{},
		EstimatedTotalHits: 0,
		Offset:             0,
		Limit:              20,
	}

	// when
	data := readSearchResultsAndPrepareData("my_query", &response)

	// then
	assert.Equal(t, "3", data.ThumbSize)
	assert.Equal(t, "/photos", data.ContextRoot)
	assert.Equal(t, true, data.Version != "")
	assert.Equal(t, "my_query", data.Query)
	assert.Equal(t, false, data.ConfByDay)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "", data.Next)
	assert.Equal(t, "tags/", data.TagsLink)
	assert.Equal(t, false, data.IsInTagFolder)
	assert.Equal(t, false, data.IsRootTagFolder)
	assert.Equal(t, "", data.Folder)
	assert.Equal(t, 0, data.Total)
	assert.Equal(t, "Recherche my_query", data.Title)
	assert.Equal(t, "Aucun résultat de recherche", data.Message)
	assert.True(t, data.IsRoot)
	assert.Equal(t, 0, len(data.GroupsName))
	assert.Equal(t, 0, len(data.Folders))

	assertSearchResults(t, data.SearchResults, "my_query,")
}

func TestComputeTopLinks_searchResults_middle(t *testing.T) {
	// given
	folderCurrent := "2023/01-14_AlbumNumber2"
	folderFullPath := "fixtures/2023/01-14_AlbumNumber2"
	conf.Config.Images = "fixtures/"
	expectedSearchResults := base64.URLEncoding.EncodeToString([]byte("my_query,2018/05-06_Other2,2023/01-04_AlbumNumber1,2023/01-14_AlbumNumber2,2022/12-01_LastMonth,2019/06-06_Other,"))
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, expectedSearchResults, &data)

	// then
	assert.Equal(t, "my_query", data.Query)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "2023/01-04_AlbumNumber1"+searchQueryUri+expectedSearchResults, data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "2022/12-01_LastMonth"+searchQueryUri+expectedSearchResults, data.Next)
}

func TestComputeTopLinks_searchResults_first(t *testing.T) {
	// given
	folderCurrent := "2023/01-14_AlbumNumber2"
	folderFullPath := "fixtures/2023/01-14_AlbumNumber2"
	conf.Config.Images = "fixtures/"
	expectedSearchResults := base64.URLEncoding.EncodeToString([]byte("my_query,2023/01-14_AlbumNumber2,2022/12-01_LastMonth,2019/06-06_Other,2018/05-06_Other2,2023/01-04_AlbumNumber1,"))
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, expectedSearchResults, &data)

	// then
	assert.Equal(t, "my_query", data.Query)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "2022/12-01_LastMonth"+searchQueryUri+expectedSearchResults, data.Next)
}

func TestComputeTopLinks_searchResults_last(t *testing.T) {
	// given
	folderCurrent := "2023/01-14_AlbumNumber2"
	folderFullPath := "fixtures/2023/01-14_AlbumNumber2"
	conf.Config.Images = "fixtures/"
	expectedSearchResults := base64.URLEncoding.EncodeToString([]byte("my_query,2019/06-06_Other,2018/05-06_Other2,2023/01-04_AlbumNumber1,2023/01-14_AlbumNumber2,"))
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, expectedSearchResults, &data)

	// then
	assert.Equal(t, "my_query", data.Query)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "2023/01-04_AlbumNumber1"+searchQueryUri+expectedSearchResults, data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "", data.Next)
}

func TestComputeTopLinks_searchResults_no_results(t *testing.T) {
	// given
	folderCurrent := "2023/01-14_AlbumNumber2"
	folderFullPath := "fixtures/2023/01-14_AlbumNumber2"
	conf.Config.Images = "fixtures/"
	expectedSearchResults := base64.URLEncoding.EncodeToString([]byte("no"))
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, expectedSearchResults, &data)

	// then
	assert.Equal(t, "no", data.Query)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "", data.Next)
}

func TestComputeTopLinks_searchResults_incorrect1(t *testing.T) {
	// given
	folderCurrent := "2023/01-14_AlbumNumber2"
	folderFullPath := "fixtures/2023/01-14_AlbumNumber2"
	conf.Config.Images = "fixtures/"
	expectedSearchResults := base64.URLEncoding.EncodeToString([]byte("my_query,2019/06-06_Other"))
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, expectedSearchResults, &data)

	// then
	assert.Equal(t, "my_query", data.Query)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "", data.Next)
}

func TestComputeTopLinks_searchResults_incorrect3(t *testing.T) {
	// given
	folderCurrent := "2023/01-14_AlbumNumber2"
	folderFullPath := "fixtures/2023/01-14_AlbumNumber2"
	conf.Config.Images = "fixtures/"
	expectedSearchResults := "incorrect"
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, expectedSearchResults, &data)

	// then
	assert.Equal(t, "", data.Query)
	assert.Equal(t, 0, len(data.Breadcrumb))
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "", data.Next)
}

func TestComputeTopLinks_readFolder_root(t *testing.T) {
	// given
	folderCurrent := "2023"
	folderFullPath := "fixtures/2023"
	conf.Config.Images = "fixtures/"
	searchResults := ""
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, searchResults, &data)

	// then
	assert.Equal(t, "", data.Query)
	assert.Equal(t, []Item{{Name: "2023"}}, data.Breadcrumb)
	assert.Equal(t, "2022", data.Previous)
	assert.Equal(t, "", data.Up)
	assert.Equal(t, "2024", data.Next)
}

func TestComputeTopLinks_readFolder_first(t *testing.T) {
	// given
	folderCurrent := "2023/01-04_AlbumNumber1"
	folderFullPath := "fixtures/2023/01-04_AlbumNumber1"
	conf.Config.Images = "fixtures/"
	searchResults := ""
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, searchResults, &data)

	// then
	assert.Equal(t, "", data.Query)
	assert.Equal(t, []Item{{Link: "2023", Name: "2023"}, {Link: "", Name: "01-04_AlbumNumber1"}}, data.Breadcrumb)
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "2023", data.Up)
	assert.Equal(t, "2023/01-14_AlbumNumber2", data.Next)
}

func TestComputeTopLinks_readFolder_middle(t *testing.T) {
	// given
	folderCurrent := "2023/01-14_AlbumNumber2"
	folderFullPath := "fixtures/2023/01-14_AlbumNumber2"
	conf.Config.Images = "fixtures/"
	searchResults := ""
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, searchResults, &data)

	// then
	assert.Equal(t, "", data.Query)
	assert.Equal(t, []Item{{Link: "2023", Name: "2023"}, {Link: "", Name: "01-14_AlbumNumber2"}}, data.Breadcrumb)
	assert.Equal(t, "2023/01-04_AlbumNumber1", data.Previous)
	assert.Equal(t, "2023", data.Up)
	assert.Equal(t, "2023/03-12_AnotherAlbum", data.Next)
}

func TestComputeTopLinks_readFolder_last(t *testing.T) {
	// given
	folderCurrent := "2023/06-09_MyAlbum"
	folderFullPath := "fixtures/2023/06-09_MyAlbum"
	conf.Config.Images = "fixtures/"
	searchResults := ""
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, searchResults, &data)

	// then
	assert.Equal(t, "", data.Query)
	assert.Equal(t, []Item{{Link: "2023", Name: "2023"}, {Link: "", Name: "06-09_MyAlbum"}}, data.Breadcrumb)
	assert.Equal(t, "2023/03-12_AnotherAlbum", data.Previous)
	assert.Equal(t, "2023", data.Up)
	assert.Equal(t, "", data.Next)
}

func TestComputeTopLinks_readFolder_subfolder(t *testing.T) {
	// given
	folderCurrent := "2023/06-09_MyAlbum/subfolder"
	folderFullPath := "fixtures/2023/06-09_MyAlbum/subfolder"
	conf.Config.Images = "fixtures/"
	searchResults := ""
	data := Data{}

	// when
	computeTopLinks(folderCurrent, folderFullPath, searchResults, &data)

	// then
	assert.Equal(t, "", data.Query)
	assert.Equal(t, []Item{{Link: "2023", Name: "2023"}, {Link: "2023/06-09_MyAlbum", Name: "06-09_MyAlbum"}, {Link: "", Name: "subfolder"}}, data.Breadcrumb)
	assert.Equal(t, "", data.Previous)
	assert.Equal(t, "2023/06-09_MyAlbum", data.Up)
	assert.Equal(t, "", data.Next)
}

func assertSearchResults(t assert.TestingT, searchResults string, expected string) {
	assert.True(t, strings.HasPrefix(searchResults, searchQueryUri))
	dec, err := base64.URLEncoding.DecodeString(strings.Replace(searchResults, searchQueryUri, "", 1))
	assert.Nil(t, err)
	assert.Equal(t, expected, string(dec))
}

func assertTags(t *testing.T, data Data) {
	tagsPictures := map[string][]string{"loooonnngggNaaaame": {"2023-03-12_AnotherAlbum_photo_01.jpg"}, "toPrint": {"2022-12-01_LastMonth_photo_1.jpg", "2023-01-04_AlbumNumber1_photo_03.jpg"}}
	assert.Equal(t, tagsPictures, data.TagsPictures)
	assert.Equal(t, "tags/", data.TagsLink)
	assert.Equal(t, false, data.IsInTagFolder)
	assert.Equal(t, false, data.IsRootTagFolder)
}

// Return keys of the given map
func keys[V any](m map[string]V) []string {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return keys
}
