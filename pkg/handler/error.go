package handler

import (
	"fmt"
)

func PanicOnErr(err error, format string, arg ...any) {
	if err != nil {
		msgComplete := fmt.Sprintf(format, arg...)
		panic(fmt.Sprintf("%s: %s", msgComplete, err))
	}
}
