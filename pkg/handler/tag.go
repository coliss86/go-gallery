/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/gorilla/mux"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/coliss86/go-gallery/pkg/conf"
	"gitlab.com/coliss86/go-gallery/pkg/file"
)

func ManageTag(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	policy := bluemonday.UGCPolicy()
	tag := strings.Replace(policy.Sanitize(vars["tag"]), "..", ".", -1)

	if req.Method == http.MethodPost {
		PanicOnErr(createTag(tag), "can't create tag folder '%s'", tag)
	} else if req.Method == http.MethodDelete {
		PanicOnErr(deleteTag(tag), "can't delete tag folder '%s'", tag)
	} else {
		http.Error(resp, "Method unknown", http.StatusBadRequest)
	}
}

func ManageImg(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	policy := bluemonday.UGCPolicy()
	decoder := json.NewDecoder(req.Body)
	typeImg := struct {
		Image *string `json:"img"`
	}{}

	err := decoder.Decode(&typeImg)
	if err != nil {
		// bad JSON or unrecognized json field
		http.Error(resp, err.Error(), http.StatusBadRequest)
		return
	}
	img := strings.Replace(policy.Sanitize(*typeImg.Image), "..", "", -1)
	tag := strings.Replace(policy.Sanitize(vars["tag"]), "..", ".", -1)

	if req.Method == http.MethodPost {
		PanicOnErr(addImageToTag(img, tag), "can't add the image '%s' to tag '%s'", img, tag)
	} else if req.Method == http.MethodDelete {
		PanicOnErr(deleteImageFromTag(img, tag), "can't delete the image '%s' from the tag '%s'", img, tag)
	} else {
		http.Error(resp, "Method unknown", http.StatusBadRequest)
	}
}

func addImageToTag(img string, tag string) error {
	log.Println("Copy", img, "to", tag)
	tagFolder := file.PathJoin(conf.Config.Tags, tag)
	err := os.MkdirAll(tagFolder, os.ModePerm)
	if err != nil {
		return fmt.Errorf("can't create tag folder '%s': %w", tagFolder, err)
	}

	src := file.PathJoin(conf.Config.Images, img)
	dest := file.PathJoin(tagFolder, path.Base(img))
	err = file.CopyFile(src, dest)
	if err != nil {
		return fmt.Errorf("can't copy '%s' to '%s': %w", src, dest, err)
	}
	return nil
}

func deleteImageFromTag(img string, tag string) error {
	log.Println("Delete", img, "from", tag)
	dest := file.PathJoin(conf.Config.Tags, tag, path.Base(img))
	err := os.Remove(dest)
	if err != nil {
		return fmt.Errorf("can't delete image '%s' from tag '%s': %e", img, tag, err)
	}
	return nil
}

func createTag(tag string) error {
	log.Println("Create tag", tag)
	tagFolder := file.PathJoin(conf.Config.Tags, tag)
	return os.MkdirAll(tagFolder, os.ModePerm)
}

func deleteTag(tag string) error {
	log.Println("Delete tag", tag)
	tagFolder := file.PathJoin(conf.Config.Tags, tag)
	return os.RemoveAll(tagFolder)
}

func tagList() (tags []string) {
	files, err := os.ReadDir(conf.Config.Tags)
	if err != nil {
		log.Println("Listing tags", conf.Config.Tags, err)
		return
	}

	tags = make([]string, 0)
	for _, entry := range files {
		if entry.IsDir() {
			tags = append(tags, entry.Name())
		}
	}
	return
}

func tagListPictures(tag string) (files []string) {
	fs, err := os.ReadDir(file.PathJoin(conf.Config.Tags, tag))
	if err != nil {
		log.Println("Listing images in tag ", file.PathJoin(conf.Config.Tags, tag), err)
		return
	}

	files = make([]string, 0)
	for _, entry := range fs {
		if !entry.IsDir() {
			files = append(files, entry.Name())
		}
	}
	return
}
