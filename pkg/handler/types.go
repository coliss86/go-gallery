/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"gitlab.com/coliss86/go-gallery/pkg/gallery"
)

type Item struct {
	Link     string
	Name     string
	LongName string
	Thumb    string
	Class    string
	Total    int
	Day      string
	Month    string
	Year     string
}

type Data struct {
	Title           string
	Message         string
	Breadcrumb      []Item
	Pictures        []string
	Videos          []string
	Folders         map[string]map[string][]Item
	GroupsName      map[string]string
	Folder          string
	IsRoot          bool
	IsInTagFolder   bool
	IsRootTagFolder bool
	TagsPictures    map[string][]string
	TagsLink        string
	Total           int
	Meta            gallery.Document
	ContextRoot     string
	ConfByDay       bool
	Previous        string
	Up              string
	Next            string
	ThumbSize       string
	Query           string
	Version         string
	SearchResults   string
}

var monthsName = map[string]string{"01": "Janvier", "02": "Février", "03": "Mars", "04": "Avril", "05": "Mai", "06": "Juin", "07": "Juillet", "08": "Août", "09": "Septembre", "10": "Octobre", "11": "Novembre", "12": "Décembre"}
