/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"regexp"
	"sort"
	"strings"
	"text/template"

	"github.com/earthboundkid/versioninfo/v2"

	"github.com/spf13/cast"

	"github.com/gorilla/mux"
	"github.com/meilisearch/meilisearch-go"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/coliss86/go-gallery/pkg/conf"
	"gitlab.com/coliss86/go-gallery/pkg/file"
	"gitlab.com/coliss86/go-gallery/pkg/gallery"
	"gitlab.com/coliss86/go-gallery/pkg/media"
)

// 03-01_LongDescription
var folderShortDateRE = regexp.MustCompile(`^([0-9]{4}-)?([0-9]{2})-([0-9]+)([_-](.*))?$`)
var excludeHiddenFileRE = regexp.MustCompile(`^\..*$`)
var pictureRE = regexp.MustCompile(`(?i).*\.(jpeg|jpg|gif|png|bmp)$`)
var urlFolderRE = regexp.MustCompile(`(.*)/([^/]*)/?`)
var videoRE = regexp.MustCompile(`(?i)(.*)\.(mp4|m4v|mpeg|mpg|avi)$`)

const searchQueryUri = "?search="
const limitSearch = 50

func RenderUI(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	policy := bluemonday.UGCPolicy()

	folderCurrent := strings.TrimSuffix(strings.Replace(policy.Sanitize(vars["folder"]), "..", ".", -1), "/")
	searchResults := req.FormValue("search")

	folderFullPath := file.PathJoin(conf.Config.Images, folderCurrent)

	// Return a 404 if the folder doesn't exist
	info, err := os.Stat(folderFullPath)
	if err != nil && os.IsNotExist(err) || !info.IsDir() {
		http.NotFound(resp, req)
		return
	}

	data := readFolderAndPrepareData(folderCurrent, folderFullPath, searchResults)

	go func() {
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("panic occurred: ", r)
			}
		}()
		launchResize(data, folderCurrent)
	}()

	renderTemplate(resp, data)
}

func readFolderAndPrepareData(folderCurrent string, folderFullPath string, searchResults string) Data {
	data := Data{}
	data.ThumbSize = fmt.Sprintf("%d", conf.Config.ThumbSize)
	data.ContextRoot = conf.Config.ContextRoot
	data.Version = versioninfo.Short()

	data.ConfByDay = conf.Config.ByDay
	data.GroupsName = monthsName
	if strings.HasSuffix(folderCurrent, "/") {
		data.Folder = folderCurrent
	} else if len(folderCurrent) > 0 {
		data.Folder = folderCurrent + "/"
	}

	// breadcrumb and previous/up/next links
	computeTopLinks(folderCurrent, folderFullPath, searchResults, &data)

	// pictures
	files, err := os.ReadDir(folderFullPath)
	PanicOnErr(err, "Can't list files in '%s'", folderFullPath)
	data.Folders = make(map[string]map[string][]Item)
	for _, f := range files {
		filename := f.Name()
		if !excludeHiddenFileRE.MatchString(filename) {
			if f.IsDir() {
				item := convertFolderToItem(folderFullPath, folderCurrent, filename)

				// store value by month
				_, exists := data.Folders[item.Month]
				if !exists {
					data.Folders[item.Month] = make(map[string][]Item)
				}

				day, exists := data.Folders[item.Month][item.Day]
				if !exists {
					data.Folders[item.Month][item.Day] = make([]Item, 5)
				}
				day = append(day, item)
				sort.Slice(day, func(i, j int) bool {
					return day[i].Name < day[j].Name
				})
				data.Folders[item.Month][item.Day] = day

			} else if videoRE.MatchString(filename) {
				data.Videos = append(data.Videos, filename)
			} else if pictureRE.MatchString(filename) {
				data.Pictures = append(data.Pictures, filename)
			}
		}
	}

	// metadata
	meta, _ := gallery.ReadFolderMeta(folderFullPath)
	matches := urlFolderRE.FindStringSubmatch(folderCurrent)
	title := ""
	if meta.Title != "" {
		title = meta.Title
		data.Meta = meta
	} else if len(matches) > 0 {
		title = matches[2]
	} else {
		title = folderCurrent
	}
	data.Title = title
	if title == "" {
		// root folder
		data.Title = "Gallery"
		data.Message = "Dossiers"
		data.IsRoot = true
	} else {
		data.IsRoot = false
	}
	data.Total = len(data.Videos) + len(data.Pictures)

	// tags
	data.TagsPictures = make(map[string][]string)
	tags := tagList()
	for _, t := range tags {
		data.TagsPictures[t] = tagListPictures(t)
	}

	data.TagsLink = strings.ReplaceAll(conf.Config.Tags, conf.Config.Images, "")
	data.IsInTagFolder = strings.HasPrefix(folderFullPath+"/", conf.Config.Tags)
	data.IsRootTagFolder = folderFullPath+"/" == conf.Config.Tags
	if data.IsRootTagFolder {
		data.Title = "Tags"
		data.Message = "Tags"
		data.IsRoot = true
	}
	return data
}

func Search(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	policy := bluemonday.UGCPolicy()

	query := policy.Sanitize(vars["q"])
	log.Println("Search query:", query)

	client := meilisearch.New(conf.Config.MeilisearchUrl, meilisearch.WithAPIKey(conf.Config.MeilisearchToken))
	got, err := client.Index(conf.Config.MeilisearchIndex).Search(query,
		&meilisearch.SearchRequest{
			Limit: limitSearch,
		})
	PanicOnErr(err, "Error while searching")

	data := readSearchResultsAndPrepareData(query, got)

	// final generation
	renderTemplate(resp, data)
}

func readSearchResultsAndPrepareData(query string, got *meilisearch.SearchResponse) Data {
	data := Data{}
	data.ThumbSize = fmt.Sprintf("%d", conf.Config.ThumbSize)
	data.ContextRoot = conf.Config.ContextRoot
	data.Version = versioninfo.Short()

	data.TagsLink = strings.ReplaceAll(conf.Config.Tags, conf.Config.Images, "")
	data.Query = query
	data.ConfByDay = false
	data.IsRoot = true

	total := int(got.EstimatedTotalHits)
	data.Total = total
	if total == 0 {
		data.Message = "Aucun résultat de recherche"
	} else if total == 1 {
		data.Message = fmt.Sprintf("1 résultat de recherche '%s' :", query)
	} else if total >= limitSearch {
		data.Message = fmt.Sprintf("trop de résultats de recherche (> %d) pour '%s', affiner la recherche", limitSearch, query)
	} else {
		data.Message = fmt.Sprintf("%d résultats de recherche '%s' :", total, query)
	}
	data.Title = "Recherche " + query

	searchResults := query + ","
	data.Folders = make(map[string]map[string][]Item)
	// extract results
	for _, hit := range got.Hits {
		hit := hit.(map[string]interface{})
		item := convertFolderToItem(file.PathJoin(conf.Config.Images, hit["parent"].(string)), hit["parent"].(string), hit["name"].(string))

		// store value by year
		_, exists := data.Folders[item.Year]
		if !exists {
			data.Folders[item.Year] = make(map[string][]Item)
		}

		day, exists := data.Folders[item.Year][item.Day]
		if !exists {
			data.Folders[item.Year][item.Day] = make([]Item, 5)
		}
		data.Folders[item.Year][item.Day] = append(day, item)
		searchResults += item.Link + ","
	}
	data.SearchResults = searchQueryUri + base64.URLEncoding.EncodeToString([]byte(searchResults))

	data.GroupsName = map[string]string{}
	for year := range data.Folders {
		data.GroupsName[year] = year
	}
	return data
}

func convertFolderToItem(parent string, prefixLink string, folder string) Item {
	item := Item{}
	if prefixLink == "" {
		item.Link = folder
	} else {
		item.Link = fmt.Sprintf("%s/%s", prefixLink, folder)
	}
	matches := folderShortDateRE.FindStringSubmatch(folder)
	if len(matches) > 0 {
		item.Month = matches[2]
		if conf.Config.ByDay {
			item.Day = matches[3]
			item.Class = "folder-long"
			item.Name = matches[5]
		} else {
			item.Class = "folder-short"
			item.Name = matches[3]
		}
		// matches :
		// [2020-09-04_21-07_NOAA15_51 2020- 09 04 _21-07_NOAA15_51 21-07_NOAA15_51]
		// ou
		// [03-27-trousse  03 27 -trousse trousse]
		// ou
		// [03-27  03 27  ]
		if len(matches[1]) > 3 { // 2020-
			item.LongName = matches[5]
		} else if matches[5] != "" {
			item.LongName = matches[5]
		} else {
			item.LongName = matches[3]
		}

		if item.LongName == "" {
			item.LongName = folder
		}
	} else {
		// regexp do not match
		item.Name = folder
		item.LongName = item.Name
		if len(item.Name) > 12 {
			item.Class = "folder-long"
		} else {
			item.Class = "folder-normal"
		}
	}

	meta, err := gallery.ReadFolderMeta(file.PathJoin(parent, folder))
	if err != nil {
		return item
	}
	item.LongName = strings.Replace(meta.Title, " ", "&nbsp;", -1)

	year := ""
	// dd/mm/yyyy
	dateExploded := strings.Split(meta.Date, "/")
	if len(dateExploded) > 1 {
		year = dateExploded[2]
	}
	item.Year = year

	// thumb folder
	files, err := os.ReadDir(file.PathJoin(parent, folder))
	if err != nil {
		log.Printf("Warning : can't read folder '%s' for thumb", folder)
	}

	for _, fileInfo := range files {
		if fileInfo.IsDir() {
			item.Total++
		} else if !fileInfo.IsDir() && !excludeHiddenFileRE.MatchString(fileInfo.Name()) && pictureRE.MatchString(fileInfo.Name()) {
			item.Total++
			if item.Thumb == "" {
				item.Thumb = fileInfo.Name()
				item.Class += " folder-thumb"
			}
		}
	}

	return item
}

func renderTemplate(resp http.ResponseWriter, data Data) {
	// final generation
	tpl := template.Must(template.New("gallery").Funcs(template.FuncMap{
		"add": func(i ...interface{}) int64 {
			// from https://github.com/Masterminds/sprig/blob/8cb06fe3c8b0f1163c26b0a558669da72ee14656/functions.go#L193
			var a int64 = 0
			for _, b := range i {
				a += cast.ToInt64(b)
			}
			return a
		},
	}).ParseGlob(conf.Config.BaseDir + "/template/*.tmpl"))
	err := tpl.ExecuteTemplate(resp, "gallery.tmpl", data)
	PanicOnErr(err, "Can't render final page")
}

func computeTopLinks(folderCurrent string, folderFullPath string, searchResults string, data *Data) {
	if searchResults != "" {
		// navigation to next/previous search results
		uDec, err := base64.URLEncoding.DecodeString(searchResults)
		if err != nil {
			log.Printf("error decoding search results, ignoring: %s\n", err)
		} else {
			policy := bluemonday.UGCPolicy()
			result := policy.Sanitize(string(uDec))
			results := strings.Split(result, ",")
			if len(results) > 0 {
				data.Query = results[0]
			}
			if len(results) >= 4 {
				for pos, res := range results[1:] {
					if res == folderCurrent {
						if pos < len(results)-1 && results[pos+2] != "" {
							data.Next = results[pos+2] + searchQueryUri + searchResults
						}
						if pos > 0 {
							data.Previous = results[pos] + searchQueryUri + searchResults
						}
						break
					}
				}
			}
		}
	} else if folderCurrent != "" {
		// breadcrumb
		var breadcrumb []Item
		splits := strings.Split(folderCurrent, "/")
		for i, split := range splits {
			if len(split) > 0 {
				item := Item{}
				item.Name = split
				if i == 0 {
					item.Link = split
				} else {
					item.Link = breadcrumb[i-1].Link + "/" + split
				}
				breadcrumb = append(breadcrumb, item)
			}
		}
		breadcrumb[len(breadcrumb)-1].Link = ""

		data.Breadcrumb = breadcrumb

		// navigation to sibling folders
		parentFolder := path.Dir(folderFullPath)
		parentFolderFiles, err := os.ReadDir(parentFolder)
		PanicOnErr(err, "Can't read folder '%s'", parentFolder)

		var parentDirs []string
		for i := range parentFolderFiles {
			if parentFolderFiles[i].IsDir() {
				parentDirs = append(parentDirs, parentFolderFiles[i].Name())
			}
		}
		navParent := strings.Replace(parentFolder+"/", conf.Config.Images, "", 1)
		data.Up = strings.TrimSuffix(navParent, "/")

		for index, parentDir := range parentDirs {
			if file.PathJoin(navParent, parentDir) == folderCurrent {
				if index > 0 {
					data.Previous = parentDirs[index-1]
					if navParent != "" {
						data.Previous = navParent + data.Previous
					}

				}
				if index+1 < len(parentDirs) {
					data.Next = parentDirs[index+1]
					if navParent != "" {
						data.Next = navParent + data.Next
					}
				}
				break
			}
		}
	}
}

type Resize struct {
	picture string
}

func (r Resize) Work() {
	_, err := media.ResizeImage(r.picture, r.picture, conf.SmallCacheDir, media.ConvertSmall)
	if err != nil {
		log.Printf("can't resize image to small '%s': %s\n", r.picture, err)
	}
}

func launchResize(data Data, folderCurrent string) {
	// launch resize in background
	if len(data.Pictures) > 0 {
		var jobs []media.Job
		for _, entry := range data.Pictures {
			picture := file.PathJoin(folderCurrent, entry)
			jobs = append(jobs, Resize{picture})
		}
		media.AsyncWork(jobs)
	}
}
