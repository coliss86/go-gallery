/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/coliss86/go-gallery/pkg/conf"
	"gitlab.com/coliss86/go-gallery/pkg/file"
	"gitlab.com/coliss86/go-gallery/pkg/search"
)

func IndexAll(resp http.ResponseWriter, req *http.Request) {
	log.Println("Index all started")
	err := search.IndexAllFolders()
	PanicOnErr(err, "meilisearch reindex error")
	log.Println("Index all finished")
}

func IndexOne(resp http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	policy := bluemonday.UGCPolicy()

	folder := file.PathJoin(conf.Config.Images, policy.Sanitize(vars["folder"]))
	log.Println(folder)
	// Return a 404 if the folder doesn't exist
	info, err := os.Stat(folder)
	if err != nil && os.IsNotExist(err) || !info.IsDir() {
		http.NotFound(resp, req)
		return
	}

	err = search.IndexOneFolder(folder)
	PanicOnErr(err, "meilisearch reindex error on folder '%s'", folder)
}
