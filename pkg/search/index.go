/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package search

import (
	"fmt"
	"log"
	"os"
	"path"
	"regexp"

	"github.com/meilisearch/meilisearch-go"
	"gitlab.com/coliss86/go-gallery/pkg/conf"
	"gitlab.com/coliss86/go-gallery/pkg/gallery"
)

var excludeRE = regexp.MustCompile(`^\..*$`)

const batchSize = 10

func CheckMeilisearch() error {
	client := meilisearch.New(conf.Config.MeilisearchUrl, meilisearch.WithAPIKey(conf.Config.MeilisearchToken))
	defer client.Close()

	version, err := client.Version()
	if err != nil {
		return err
	}
	log.Printf("Meilisearch on %s version: %s", conf.Config.MeilisearchUrl, version.PkgVersion)
	return nil
}

func IndexAllFolders() error {
	log.Printf("Indexing all folders '%s' in index '%s'\n", conf.Config.Images, conf.Config.MeilisearchIndex)
	client := meilisearch.New(conf.Config.MeilisearchUrl, meilisearch.WithAPIKey(conf.Config.MeilisearchToken))
	defer client.Close()

	if err := deleteAndCreateIndex(client, conf.Config.MeilisearchIndex); err != nil {
		return fmt.Errorf("can't delete and create index '%s': %w", conf.Config.MeilisearchIndex, err)
	}

	if err := indexFolder(conf.Config.Images, client.Index(conf.Config.MeilisearchIndex)); err != nil {
		return fmt.Errorf("can't index folder '%s': %w", conf.Config.Images, err)
	}
	return nil
}

func IndexOneFolder(folder string) error {
	log.Printf("Indexing folder '%s' in index '%s'\n", folder, conf.Config.MeilisearchIndex)
	document, err := gallery.ReadFolderMeta(folder)
	if err != nil {
		return fmt.Errorf("error reading meta from folder '%s': %w", folder, err)
	}
	if document.Name == "" {
		return fmt.Errorf("nothing to index in folder '%s'", folder)
	}
	client := meilisearch.New(conf.Config.MeilisearchUrl, meilisearch.WithAPIKey(conf.Config.MeilisearchToken))
	defer client.Close()
	index := client.Index(conf.Config.MeilisearchIndex)
	task, err := index.AddDocuments(document)
	if err != nil {
		return fmt.Errorf("can't index folder '%s': %w", folder, err)
	}
	if err = waitForTask(task, client); err != nil {
		return err
	}
	return nil
}

func deleteAndCreateIndex(client meilisearch.ServiceManager, indexName string) error {
	indexes, err := client.ListIndexes(nil)
	if err != nil {
		return err
	}
	idxFound := false
	if indexes != nil {
		for _, ir := range indexes.Results {
			if ir.UID == indexName {
				idxFound = true
				break
			}
		}
	}

	if idxFound {
		task, err := client.DeleteIndex(indexName)
		if err != nil {
			return err
		}
		if err = waitForTask(task, client); err != nil {
			return err
		}
	}
	task, err := client.CreateIndex(&meilisearch.IndexConfig{
		Uid: indexName,
	})
	if err != nil {
		return err
	}

	if err = waitForTask(task, client); err != nil {
		return err
	}
	return nil
}

func waitForTask(task *meilisearch.TaskInfo, client meilisearch.ServiceManager) error {
	if task != nil {
		gotTask, err := client.WaitForTask(task.TaskUID, 0)
		if err != nil {
			return err
		}
		if gotTask.Status != meilisearch.TaskStatusSucceeded {
			return fmt.Errorf("the meilisearch's task is not succeeded '%s'", gotTask.Status)
		}
	}
	return nil
}

func indexFolder(folder string, index meilisearch.IndexManager) error {
	log.Println("Reading folder", folder)
	files, err := os.ReadDir(folder)
	if err != nil {
		return fmt.Errorf("error while indexing '%s': %w", folder, err)
	}

	var documents []gallery.Document
	for idx, entry := range files {
		if !excludeRE.MatchString(entry.Name()) && entry.IsDir() {
			_, err := os.Stat(path.Join(folder, entry.Name(), "meta.properties"))
			if err != nil && os.IsNotExist(err) {
				err := indexFolder(path.Join(folder, entry.Name()), index)
				if err != nil {
					return err
				}
				continue
			} else if err == nil {
				document, err := gallery.ReadFolderMeta(path.Join(folder, entry.Name()))
				if err != nil {
					return fmt.Errorf("error reading meta from folder '%s/%s': %w", folder, entry.Name(), err)
				}
				if document.Name == "" {
					continue
				}
				log.Println(" To index:", document.Id)
				documents = append(documents, document)
				if idx%batchSize == batchSize-1 {
					log.Println(" Send batch")
					_, err := index.AddDocumentsInBatches(documents, batchSize)
					if err != nil {
						return fmt.Errorf("error while sending data to meilisearch: %w", err)
					}
					documents = nil
				}
			}
		}
	}
	if len(documents) > 0 {
		log.Println("Send remaining batch", len(documents))
		_, err := index.AddDocumentsInBatches(documents, len(documents))
		if err != nil {
			return fmt.Errorf("error while sending data to meilisearch: %w", err)
		}
	}
	return nil
}
