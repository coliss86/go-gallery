package conf

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadPropertiesFile(t *testing.T) {
	props, err := ReadPropertiesFile("fixtures/sample_test.properties")
	if err != nil {
		t.Error("Error while reading properties file")
	}

	assert.Equal(t, "My album !", props["title"])
	assert.Equal(t, "description", props["descript"])
	assert.Equal(t, "29/12/2006", props["date"])
	assert.Equal(t, "Somewhere", props["lieu"])
	assert.Equal(t, "Bill", props["personnes"])
	assert.Equal(t, "Cat1, Cat2, cat3", props["categories"])
}

func TestReadPropertiesFile_Multiline(t *testing.T) {
	props, err := ReadPropertiesFile("fixtures/sample_test_multilines.properties")
	if err != nil {
		t.Error("Error while reading properties file")
	}
	assert.Equal(t, "My album !", props["title"])
	assert.Equal(t, "description\nmulti line !\nanother line\nforth line", props["descript"])
	assert.Equal(t, "29/12/2006", props["date"])
	assert.Equal(t, "Somewhere", props["lieu"])
	assert.Equal(t, "Bill", props["personnes"])
	assert.Equal(t, "Cat1, Cat2, cat3", props["categories"])
}

func TestReadProperties_Empty(t *testing.T) {
	_, err := ReadPropertiesFile("")
	assert.EqualError(t, err, "filename is empty")
}

func TestReadProperties_Nonexistent(t *testing.T) {
	_, err := ReadPropertiesFile("no_file")
	assert.EqualError(t, err, "open no_file: no such file or directory")
}
