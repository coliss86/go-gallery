/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package conf

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var Config Configuration

func Read(file string) error {
	propertiesFile, err := ReadPropertiesFile(file)
	if err != nil {
		return err
	}

	// parse options
	Config.Images = propertiesFile["images"]
	if propertiesFile["images"] == "" {
		return fmt.Errorf("error: option 'images' must be specified")
	}
	if !strings.HasSuffix(Config.Images, "/") {
		Config.Images += "/"
	}

	Config.Tags = propertiesFile["tags"]
	if Config.Tags == "" {
		Config.Tags = Config.Images + "/export/"
	}
	if !strings.HasSuffix(Config.Tags, "/") {
		Config.Tags += "/"
	}

	Config.Cache = propertiesFile["cache"]
	if Config.Cache == "" {
		return fmt.Errorf("error: option 'cache' must be specified")
	}
	if !strings.HasSuffix(Config.Cache, "/") {
		Config.Cache += "/"
	}

	if propertiesFile["smallSize"] != "" {
		Config.SmallSize, err = strconv.Atoi(propertiesFile["smallSize"])
		if err != nil {
			return fmt.Errorf("error parsing 'smallSize' option: %w", err)
		}
	} else {
		Config.SmallSize = 1200
	}

	if propertiesFile["thumbSize"] != "" {
		Config.ThumbSize, err = strconv.Atoi(propertiesFile["thumbSize"])
		if err != nil {
			return fmt.Errorf("error parsing 'thumbSize' option: %w", err)
		}
	} else {
		Config.ThumbSize = 150
	}

	if propertiesFile["smallResizeWorker"] != "" {
		Config.SmallResizeWorker, err = strconv.Atoi(propertiesFile["smallResizeWorker"])
		if err != nil {
			return fmt.Errorf("error parsing 'smallResizeWorker' option: %w", err)
		}
	} else {
		Config.SmallResizeWorker = 3
	}

	Config.ContextRoot = propertiesFile["contextRoot"]

	if propertiesFile["byDay"] != "" {
		Config.ByDay, err = strconv.ParseBool(propertiesFile["byDay"])
		if err != nil {
			return fmt.Errorf("error parsing 'byDay' option: %w", err)
		}
	} else {
		Config.ByDay = false
	}

	if propertiesFile["listen"] == "" {
		Config.Listen = "localhost"
	} else {
		Config.Listen = propertiesFile["listen"]
	}

	if propertiesFile["port"] != "" {
		Config.Port, err = strconv.Atoi(propertiesFile["port"])
		if err != nil {
			return fmt.Errorf("error parsing 'port' option: %w", err)
		}
	} else {
		Config.Port = 9090
	}

	Config.MeilisearchUrl = propertiesFile["meilisearchUrl"]
	if Config.MeilisearchUrl == "" {
		Config.MeilisearchUrl = "http://localhost:7700"
	}
	Config.MeilisearchToken = propertiesFile["meilisearchToken"]
	Config.MeilisearchIndex = propertiesFile["meilisearchIndex"]
	if Config.MeilisearchIndex == "" {
		Config.MeilisearchIndex = "go-gallery"
	}
	if propertiesFile["meilisearchActivated"] != "" {
		Config.MeilisearchActivated, err = strconv.ParseBool(propertiesFile["meilisearchActivated"])
		if err != nil {
			return fmt.Errorf("error parsing 'meilisearchActivated' option: %w", err)
		}
	} else {
		Config.MeilisearchActivated = false
	}

	// summary
	log.Println("Images directory", Config.Images)
	log.Println("Tags directory", Config.Tags)

	return nil
}

// ReadPropertiesFile source https://stackoverflow.com/a/46860900
func ReadPropertiesFile(filename string) (map[string]string, error) {
	config := map[string]string{}

	if len(filename) == 0 {
		return config, fmt.Errorf("filename is empty")
	}
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var read = true
	for {
		if read && !scanner.Scan() {
			break
		}
		read = true
		line := scanner.Text()
		equal := strings.Index(line, "=")
		if equal >= 0 {
			if key := strings.TrimSpace(line[:equal]); len(key) > 0 {

				if len(line) > equal {
					value := strings.TrimSpace(line[equal+1:])
					// search for multi line values
					for scanner.Scan() {
						line = scanner.Text()
						if strings.Contains(line, "=") {
							read = false
							break
						} else {
							value += "\n"
							value += strings.TrimSpace(line)
						}
					}
					config[key] = value
				}
			}
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("error reading file '%s': %w", filename, err)
	}

	return config, nil
}
