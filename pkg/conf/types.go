/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package conf

type Configuration struct {
	BaseDir              string
	Images               string
	Cache                string
	Tags                 string
	Port                 int
	SmallSize            int
	ThumbSize            int
	SmallResizeWorker    int
	ContextRoot          string
	ByDay                bool
	Listen               string
	MeilisearchActivated bool
	MeilisearchToken     string
	MeilisearchUrl       string
	MeilisearchIndex     string
}

const SmallCacheDir = "small"
const ThumbCacheDir = "thumb"
